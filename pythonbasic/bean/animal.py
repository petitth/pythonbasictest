class Animal:
    name = ''
    owners = []

    def __init__(self, name, owners):
        self.name = name
        self.owners = owners

    def scream(self):
        return '??'


class Dog(Animal):
    favorite_food = 'meat'

    def scream(self):
        return "waf waf"


class Cat(Animal):
    favorite_food = 'fish'

    def scream(self):
        return "miaou"
