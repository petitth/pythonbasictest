import unittest
from pythonbasic.file import file_util
from pythonbasic.database import sqlite_util


class TestSqlite(unittest.TestCase):

    def test_sqlite(self):
        file_util.delete_directory("./tmp")
        file_util.create_directory("./tmp")

        query_creation = file_util.get_file_content("./data/customers.sql")
        sqlite_util.execute_update("./tmp/customers.db", query_creation)

        list_germans = sqlite_util.execute_select("./tmp/customers.db",
                                                  "SELECT * FROM Customers WHERE Country = 'Germany'")
        for customer in list_germans:
            print("contact name = {0} - city = {1}".format(customer["ContactName"], customer["City"]))
        print("there are {} germans customers".format(str(len(list_germans))))

        file_util.delete_directory("./tmp")


if __name__ == "__main__":
    unittest.main()
