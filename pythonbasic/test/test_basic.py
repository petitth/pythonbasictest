import unittest


class TestBasic(unittest.TestCase):

    def test_addition(self):
        x = 8
        y = 10
        self.assertEqual(str(18), str(x + y))
        self.assertEqual(18, x + y)
        self.assertEqual(18, int("10") + int("8"))
        self.assertTrue(x + y > x)


if __name__ == "__main__":
    unittest.main()
