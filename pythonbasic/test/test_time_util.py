import unittest
from pythonbasic.time import time_util


class TestTimeUtil(unittest.TestCase):

    def test_current_date(self):
        current_date_basic = time_util.current_date()
        print(current_date_basic)
        self.assertTrue(current_date_basic is not None)

        current_date_format = time_util.current_date('%Y-%m-%d %H:%M:%S')
        print(current_date_format)
        self.assertTrue(current_date_format is not None)


if __name__ == "__main__":
    unittest.main()
