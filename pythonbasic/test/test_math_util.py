import unittest
from pythonbasic.math import math_util


class TestMathUtil(unittest.TestCase):

    def test_factorial(self):
        self.assertEqual(1, math_util.factorial(0))
        self.assertEqual(1, math_util.factorial(1))
        self.assertEqual(2, math_util.factorial(2))
        self.assertEqual(6, math_util.factorial(3))
        self.assertEqual(24, math_util.factorial(4))
        self.assertEqual(120, math_util.factorial(5))
        self.assertEqual(720, math_util.factorial(6))
        self.assertEqual(5040, math_util.factorial(7))
        self.assertEqual(40320, math_util.factorial(8))

    def test_fibonacci(self):
        self.assertEqual(1, math_util.fibonacci(1))
        self.assertEqual(1, math_util.fibonacci(2))
        self.assertEqual(2, math_util.fibonacci(3))
        self.assertEqual(3, math_util.fibonacci(4))
        self.assertEqual(5, math_util.fibonacci(5))
        self.assertEqual(8, math_util.fibonacci(6))
        self.assertEqual(13, math_util.fibonacci(7))
        self.assertEqual(21, math_util.fibonacci(8))


if __name__ == "__main__":
    unittest.main()