import unittest
from pythonbasic.math import weight_util


class TestWeightUtil(unittest.TestCase):

    def test_kilogram_to_pound(self):
        self.assertEqual(2.204623, round(weight_util.kilogram_to_pound(1), 6))
        self.assertEqual(4.409245, round(weight_util.kilogram_to_pound(2), 6))
        self.assertEqual(22.046226, round(weight_util.kilogram_to_pound(10), 6))

    def test_pound_to_kilogram(self):
        self.assertEqual(0.453592, round(weight_util.pound_to_kilogram(1), 6))
        self.assertEqual(0.907185, round(weight_util.pound_to_kilogram(2), 6))
        self.assertEqual(22.679619, round(weight_util.pound_to_kilogram(50), 6))


if __name__ == "__main__":
    unittest.main()
