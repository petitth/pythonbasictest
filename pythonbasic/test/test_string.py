import unittest
from pythonbasic.string import string_util


class TestString(unittest.TestCase):

    def setUp(self):
        # global variable
        self.STR_LEN = 10

    def test_substring(self):
        str = "hello world"
        # start from left, from character x
        self.assertEqual(str[0:], "hello world")
        self.assertEqual(str[6:], "world")
        # stops at character x
        self.assertEqual(str[:5], "hello")

        # start from the end
        self.assertEqual(str[-5:], "world")
        self.assertEqual(str[:-6], "hello")

        # from ... to ...
        self.assertEqual(str[3:-2], "lo wor")
        self.assertEqual(str[0:5], "hello")
        self.assertEqual(str[6:len(str)], "world")

    def test_format(self):
        self.assertEqual("Hello {}".format("Bob"), "Hello Bob")
        self.assertEqual("Hello {0}".format("Bob"), "Hello Bob")
        self.assertEqual("Hello {name}".format(name="Bob"), "Hello Bob")
        self.assertEqual("5 + 8 = {}".format(5 + 8), "5 + 8 = 13")

    def test_string_buffer(self):
        sql = []
        sql.append(""" select USER."NAME" last_name """)
        sql.append(""" from "USERS" USER """)
        sql.append(""" order by USER."NAME" """)
        self.assertEqual("".join(sql), """ select USER."NAME" last_name  from "USERS" USER  order by USER."NAME" """)

    def test_basic_operations(self):
        str = "test"
        self.assertEqual(len(str), 4)

        str = "hello world"
        self.assertEqual(str.upper(), "HELLO WORLD")
        self.assertEqual(str.lower(), "hello world")

        str = " hello "
        self.assertEqual(str.strip(), "hello")

        self.assertTrue(str.strip().startswith("hell"))

    def test_random_upper(self):
        str_random_upper = string_util.random_upper(self.STR_LEN)
        self.assertEqual(len(str_random_upper), self.STR_LEN)

    def test_random_lower(self):
        str_random_lower = string_util.random_lower(self.STR_LEN)
        self.assertEqual(len(str_random_lower), self.STR_LEN)

    def test_random_string(self):
        str_random_str = string_util.random_string(self.STR_LEN)
        self.assertEqual(len(str_random_str), self.STR_LEN)

    def test_random_number(self):
        str_random_num = string_util.random_number(self.STR_LEN)
        self.assertEqual(len(str_random_num), self.STR_LEN)


if __name__ == "__main__":
    unittest.main()
