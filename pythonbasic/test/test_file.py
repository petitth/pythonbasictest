import unittest
import tempfile
from pathlib import Path
from os.path import expanduser
from pythonbasic.file import file_util


class TestFile(unittest.TestCase):

    def test_basic(self):
        print("temp folder = " + tempfile.gettempdir())
        print("user folder (all platforms) = " + expanduser("~"))
        print("user folder (> Python 3.5) = " + str(Path.home()))

    def test_io(self):
        file_util.delete_directory("./tmp")
        file_util.create_directory("./tmp")
        file_util.create_directory("./tmp/something")
        file_util.create_directory("./tmp/something/v1")
        file_util.create_directory("./tmp/something/v2")
        self.assertTrue(file_util.does_path_exist("./tmp"))
        self.assertTrue(file_util.does_directory_exist("./tmp"))

        file_util.create("./tmp/something/v2/test.txt", "test 1 2 3")
        file_util.create("./tmp/color.txt", "red")
        file_util.append_content("./tmp/color.txt", "green")
        file_util.append_lines("./tmp/color.txt", ["blue", "yellow"])
        file_util.create_from_lines("./tmp/something/weather.txt", ["sun", "rain", "snow"])
        self.assertEqual(3, len(file_util.get_file_lines("./tmp/something/weather.txt")))
        self.assertEqual("test 1 2 3\n", file_util.get_file_content("./tmp/something/v2/test.txt"))

        # list folder content
        list_elts = file_util.list_content("./tmp")
        print("-- all elements from folder --")
        for elt in list_elts:
            print(elt)
        self.assertEqual(6, len(list_elts))

        # list all files from folder
        list_files = file_util.list_files("./tmp")
        print("\n-- all files from folder --")
        for elt in list_files:
            print(elt)
        self.assertEqual(3, len(list_files))

        file_util.delete_file("./tmp/color.txt")
        self.assertFalse(file_util.does_file_exist("./tmp/color.txt"))
        file_util.delete_directory("./tmp")
        self.assertFalse(file_util.does_path_exist("./tmp"))
        self.assertFalse(file_util.does_directory_exist("./tmp"))

    def test_zip_archives(self):
        file_util.delete_directory("./tmp")
        file_util.create_directory("./tmp")
        file_util.create_directory("./tmp/input/abc")
        file_util.create_directory("./tmp/input/def")
        file_util.create_directory("./tmp/input/ghi/jkl")
        file_util.create_directory("./tmp/zip")
        file_util.create_directory("./tmp/unzip")
        file_util.create("./tmp/input/abc/file1.txt", "hello world")
        file_util.create("./tmp/input/def/file2.txt", "hello again")
        file_util.create("./tmp/input/ghi/jkl/empty", "")
        file_util.create("./tmp/input/weather", "it is raining")

        file_util.zip_files("./tmp/input", "./tmp/zip/result.zip")
        self.assertTrue(file_util.does_path_exist("./tmp/zip/result.zip"))

        file_util.unzip_files("./tmp/zip/result.zip", "./tmp/unzip/all")
        self.assertTrue(file_util.does_path_exist("./tmp/unzip/all/abc/file1.txt"))
        self.assertTrue(file_util.does_path_exist("./tmp/unzip/all/def/file2.txt"))
        self.assertTrue(file_util.does_path_exist("./tmp/unzip/all/ghi/jkl/empty"))
        self.assertTrue(file_util.does_path_exist("./tmp/unzip/all/weather"))
        self.assertEqual("hello world\n", file_util.get_file_content("./tmp/unzip/all/abc/file1.txt"))
        self.assertEqual("hello again\n", file_util.get_file_content("./tmp/unzip/all/def/file2.txt"))
        file_util.delete_directory("./tmp")

    def test_subfolders(self):
        file_util.delete_directory("./tmp")
        file_util.create_directory("./tmp")
        file_util.create_directory("./tmp/abc/123/def/456")
        file_util.create("./tmp/ghi/jkl/mno/file", "hello world")
        file_util.create_from_lines("./tmp/suba/subb/color", ["red", "green", "blue"])
        self.assertTrue(file_util.does_file_exist("./tmp/ghi/jkl/mno/file"))
        self.assertTrue(file_util.does_file_exist("./tmp/suba/subb/color"))
        file_util.delete_directory("./tmp")

    def test_json(self):
        file_util.delete_directory("./tmp")
        file_util.create_directory("./tmp")

        data = {}
        data["first_name"] = "John"
        data["last_name"] = "Smith"
        file_util.write_json(data, "./tmp/output/person.json")
        self.assertTrue(file_util.does_path_exist("./tmp/output/person.json"))

        read_json = file_util.read_json("./tmp/output/person.json")
        self.assertEqual("John", read_json["first_name"])
        self.assertEqual("Smith", read_json["last_name"])

        file_util.delete_directory("./tmp")

    def test_retrieving_from_url(self):
        content = file_util.get_file_content_from_url(
            "https://gitlab.com/petitth/pythonbasictest/-/raw/master/data/foo")
        self.assertEqual("bar", content)


if __name__ == "__main__":
    unittest.main()
