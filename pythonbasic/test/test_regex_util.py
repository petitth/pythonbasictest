import unittest
from pythonbasic.string import regex_util


class TestRegexUtil(unittest.TestCase):

    def test_is_only_numeric(self):
        self.assertEqual(True, regex_util.is_only_numeric("123456"))
        self.assertEqual(True, regex_util.is_only_numeric("987456"))
        self.assertEqual(True, regex_util.is_only_numeric("55555555555555"))

        self.assertEqual(False, regex_util.is_only_numeric("123456a"))
        self.assertEqual(False, regex_util.is_only_numeric("e123456"))
        self.assertEqual(False, regex_util.is_only_numeric("123y456"))

        self.assertEqual(False, regex_util.is_only_numeric("abcdef"))
        self.assertEqual(False, regex_util.is_only_numeric("123 456"))
        self.assertEqual(False, regex_util.is_only_numeric("123_456"))
        self.assertEqual(False, regex_util.is_only_numeric("1234#56"))
        self.assertEqual(False, regex_util.is_only_numeric("1.23456"))

    def test_is_only_alphanumeric(self):
        self.assertEqual(True, regex_util.is_only_alphanumeric("123456"))
        self.assertEqual(True, regex_util.is_only_alphanumeric("abcdef"))
        self.assertEqual(True, regex_util.is_only_alphanumeric("abcd789"))
        self.assertEqual(True, regex_util.is_only_alphanumeric("456azerty"))

        self.assertEqual(False, regex_util.is_only_alphanumeric("123#456"))
        self.assertEqual(False, regex_util.is_only_alphanumeric("123_456"))
        self.assertEqual(False, regex_util.is_only_alphanumeric("bla.bla.bla"))
        self.assertEqual(False, regex_util.is_only_alphanumeric("$omething"))
        self.assertEqual(False, regex_util.is_only_alphanumeric("_anything"))
        self.assertEqual(False, regex_util.is_only_alphanumeric("really_anything"))

    def test_is_only_numeric_and_alphanumeric(self):
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("a1"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("abcdef1"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("2abcdef"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("abc3def"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("4abc5def6"))

        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("a123456"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("123456z"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("123e456"))
        self.assertEqual(True, regex_util.is_only_numeric_and_alphanumeric("a123e456z"))

        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("123456"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("abcdef"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("abcdef_"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("abc#def"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric(".abcdef"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("abc#123"))
        self.assertEqual(False, regex_util.is_only_numeric_and_alphanumeric("_abc123"))

    def test_contains_only_repeating_string(self):
        self.assertEqual(True, regex_util.contains_only_repeating_string("XXXXXXXXX", "X"))
        self.assertEqual(True, regex_util.contains_only_repeating_string("ZZZZ", "Z"))
        self.assertEqual(True, regex_util.contains_only_repeating_string("ABCABCABC", "ABC"))

        self.assertEqual(False, regex_util.contains_only_repeating_string("123456", "X"))
        self.assertEqual(False, regex_util.contains_only_repeating_string("AXXXXXXXX", "X"))
        self.assertEqual(False, regex_util.contains_only_repeating_string("AXXXXXXXXA", "X"))
        self.assertEqual(False, regex_util.contains_only_repeating_string("XXXXXXXXA", "X"))
        self.assertEqual(False, regex_util.contains_only_repeating_string("azerty", "X"))


if __name__ == "__main__":
    unittest.main()
