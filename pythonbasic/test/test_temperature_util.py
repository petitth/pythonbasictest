import unittest
from pythonbasic.math import temperature_util


class TestTemperatureUtil(unittest.TestCase):

    def test_celsius_to_fahrenheit(self):
        self.assertEqual(32, temperature_util.celsius_to_fahrenheit(0))
        self.assertEqual(50, temperature_util.celsius_to_fahrenheit(10))
        self.assertEqual(68, temperature_util.celsius_to_fahrenheit(20))
        self.assertEqual(98.6, temperature_util.celsius_to_fahrenheit(37))
        self.assertEqual(104, temperature_util.celsius_to_fahrenheit(40))

    def test_fahrenheit_to_celsius(self):
        self.assertEqual(0, temperature_util.fahrenheit_to_celsius(32))
        self.assertEqual(10, temperature_util.fahrenheit_to_celsius(50))
        self.assertEqual(20, temperature_util.fahrenheit_to_celsius(68))
        self.assertEqual(37, temperature_util.fahrenheit_to_celsius(98.6))
        self.assertEqual(40, temperature_util.fahrenheit_to_celsius(104))


if __name__ == "__main__":
    unittest.main()
