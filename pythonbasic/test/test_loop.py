import unittest


class TestLoop(unittest.TestCase):

    def test_for_loop(self):
        colors = ["red", "green", "blue"]

        # basic for loop with break, will display "red"
        for x in colors:
            if x == "green":
                break
            print(x)

        print("\n")
        # basic for loop with continue, will display "red", "blue"
        for x in colors:
            if x == "green":
                continue
            print(x)

    def test_range(self):
        # will display 1, 2, 3
        for x in range(0, 3):
            print("index = %d" % (x + 1))

    def test_range_list(self):
        colors = ["red", "green", "blue"]
        for i in range(0, len(colors)):
            print("color {0} = {1}".format(str(i), colors[i]))

    def test_while(self):
        # will display 1, 2, 3
        i = 1
        while i <= 3:
            print("element %d" % (i))
            i += 1


if __name__ == "__main__":
    unittest.main()
