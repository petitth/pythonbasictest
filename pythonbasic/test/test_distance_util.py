import unittest
from pythonbasic.math import distance_util


class TestDistanceUtil(unittest.TestCase):
    def test_kilometers_to_miles(self):
        self.assertEqual(0, distance_util.kilometers_to_miles(0))
        self.assertEqual(0.6213711922, distance_util.kilometers_to_miles(1))
        self.assertEqual(1.2427423844, distance_util.kilometers_to_miles(2))

    def test_miles_to_kilometers(self):
        self.assertEqual(0, distance_util.miles_to_kilometers(0))
        self.assertEqual(1.609344, distance_util.miles_to_kilometers(1))
        self.assertEqual(3.218688, distance_util.miles_to_kilometers(2))

    def test_centimeter_to_feet(self):
        self.assertEqual(0, distance_util.centimeter_to_feet(0))
        self.assertEqual(0.032808, round(distance_util.centimeter_to_feet(1), 6))
        self.assertEqual(0.328084, round(distance_util.centimeter_to_feet(10), 6))

    def test_feet_to_centimeter(self):
        self.assertEqual(0, distance_util.feet_to_centimeter(0))
        self.assertEqual(30.48, distance_util.feet_to_centimeter(1))
        self.assertEqual(304.8, distance_util.feet_to_centimeter(10))

    def test_centimeter_to_inch(self):
        self.assertEqual(0, distance_util.centimeter_to_inch(0))
        self.assertEqual(0.393701, round(distance_util.centimeter_to_inch(1), 6))
        self.assertEqual(3.937008, round(distance_util.centimeter_to_inch(10), 6))

    def test_inch_to_centimeter(self):
        self.assertEqual(0, distance_util.inch_to_centimeter(0))
        self.assertEqual(2.54, distance_util.inch_to_centimeter(1))
        self.assertEqual(25.4, distance_util.inch_to_centimeter(10))

    def test_yard_to_meter(self):
        self.assertEqual(0, distance_util.yard_to_meter(0))
        self.assertEqual(0.9144, round(distance_util.yard_to_meter(1), 6))
        self.assertEqual(9.144, round(distance_util.yard_to_meter(10), 6))

    def test_meter_to_yard(self):
        self.assertEqual(0, distance_util.meter_to_yard(0))
        self.assertEqual(1.093613, round(distance_util.meter_to_yard(1), 6))
        self.assertEqual(10.936133, round(distance_util.meter_to_yard(10), 6))


if __name__ == "__main__":
    unittest.main()
