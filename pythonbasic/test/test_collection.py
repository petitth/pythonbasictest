import unittest


class TestCollection(unittest.TestCase):

    def test_list(self):
        colors = ["red", "green", "blue"]

        # basics
        self.assertEqual(colors[0], "red")
        self.assertEqual(len(colors), 3)
        colors.append("yellow")
        self.assertEqual(len(colors), 4)

        # remove elements
        colors.pop(0)
        self.assertEqual(colors[0], "green")
        colors.remove("yellow")
        self.assertEqual(len(colors), 2)

        # show content of array
        colors.sort()
        for x in colors:
            print(x)

        # copy an array
        same_colors = colors.copy()
        self.assertEqual("blue", colors[0])
        self.assertEqual("blue", same_colors[0])
        self.assertEqual("green", colors[1])
        self.assertEqual("green", same_colors[1])

        # add multiple elements
        colors.extend(["orange"])
        self.assertEqual(len(colors), 3)

        # count occurrences & search
        self.assertEqual(1, colors.count("orange"))
        self.assertEqual(1, colors.index("green"))
        if "orange" in colors:
            print("'orange' is in the colors list")

    def test_alternate_creation_of_list(self):
        # note the double round-brackets
        colors = list(("red", "green", "blue"))
        self.assertEqual(colors, ['red', 'green', 'blue'])
        self.assertEqual(str(colors), "['red', 'green', 'blue']")

    def test_alternate_search_list(self):
        colors = ["red", "green", "blue"]
        result = [item for item in colors if item == "red"]
        if len(result) > 0:
            print("color was found ({0})".format(result[0]))

    def test_split_string(self):
        str = "red-green-blue"
        colors = str.split("-")
        self.assertEqual(colors[1], "green")
        self.assertEqual(len(colors), 3)

    def test_join_on_list(self):
        colors = ["red", "green", "blue"]
        result = ",".join(colors)
        self.assertEqual(result, "red,green,blue")

    def test_dictionary(self):
        # set values one by one
        data = {}
        data["first_name"] = "Bob"
        data["last_name"] = "Razovsky"
        self.assertEqual(2, len(data))
        for key, val in data.items():
            print(key + " : " + val)

        # set multiple values at once
        dict = {
            "color": "black"
        }
        print(dict)
        self.assertEqual(dict.get("color", None), "black")
        self.assertEqual(dict.get("weather", None), None)


if __name__ == "__main__":
    unittest.main()
