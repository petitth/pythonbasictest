import unittest
import operator
from pythonbasic.bean.animal import *


class TestBean(unittest.TestCase):

    def test_bean(self):
        animal = Animal("Bob", [])
        self.assertEqual("Bob", animal.name)
        self.assertEqual("??", animal.scream())

        dog = Dog("Rex", [])
        self.assertEqual("Rex", dog.name)
        self.assertEqual("waf waf", dog.scream())
        self.assertEqual("meat", dog.favorite_food)

        cat = Cat("Felix", [])
        self.assertEqual("Felix", cat.name)
        self.assertEqual("miaou", cat.scream())
        self.assertEqual("fish", cat.favorite_food)

        animal_list = [animal, dog, cat]
        # sort a list
        animal_list.sort(key=operator.attrgetter("name"))
        for x in animal_list:
            print(x.name)

        # search element in list
        result = [elt for elt in animal_list if elt.name == "Felix"]
        if len(result) > 0:
            print("animal '{0}' was found".format(result[0].name))

        # extract attribute
        name_list = [x.name for x in animal_list]
        for name in name_list:
            print("name attribute = " + name)

        # variant for extracting an attribute
        food_list = map(lambda x: x.favorite_food, [cat, dog])
        for food in food_list:
            print("favorite food attribute = " + food)

    def test_extract_attribute(self):
        pass


if __name__ == "__main__":
    unittest.main()
