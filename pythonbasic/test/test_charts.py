import tempfile
import unittest

import plotly.graph_objects as go

from pythonbasic.file import file_util
from pythonbasic.time import time_util


class TestFile(unittest.TestCase):

    def test_pie_chart(self):
        current_time = time_util.current_date('%Y-%m-%d_%H%M%S')
        file_name = "{}/{}_pie_chart.jpg".format(tempfile.gettempdir(), current_time)
        labels = ['Oxygen', 'Hydrogen', 'Carbon_Dioxide', 'Nitrogen']
        values = [4500, 2500, 1053, 500]
        fig = go.Figure(data=[go.Pie(labels=labels, values=values)])
        fig.write_image(file_name)
        print("the chart has been created : {}".format(file_name))
        self.assertTrue(file_util.does_file_exist(file_name))