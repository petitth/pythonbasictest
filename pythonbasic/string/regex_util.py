import re

REGEX_ONLY_NUMERIC                  = r"^[0-9]+$"
REGEX_ONLY_ALPHANUMERIC             = r"^[a-zA-Z0-9]*$"
REGEX_ONLY_NUMERIC_AND_ALPHANUMERIC = r"^([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*$"
REGEX_ONLY_REPEATING_CHAR           = r"^[STR]*$"


''' Does an expression only contain numeric characters ? '''
def is_only_numeric(expression):
    if re.match(REGEX_ONLY_NUMERIC, expression):
        return True
    else:
        return False


''' Does an expression only contain alphanumeric characters ? '''
def is_only_alphanumeric(expression):
    if re.match(REGEX_ONLY_ALPHANUMERIC, expression):
        return True
    else:
        return False


''' Does an expression only contain alphanumeric and numeric characters (at least one of each type) ? '''
def is_only_numeric_and_alphanumeric(expression):
    if re.match(REGEX_ONLY_NUMERIC_AND_ALPHANUMERIC, expression):
        return True
    else:
        return False


''' Does an expression contain only a repeating character ? '''
def contains_only_repeating_string(expression, repeating_string):
    regex = REGEX_ONLY_REPEATING_CHAR.replace("STR", repeating_string)
    if check_regex_expression(expression, regex):
        return True
    else:
        return False


''' Does an expression respect a regex expression ? '''
def check_regex_expression(expression, regex):
    if re.match(regex, expression):
        return True
    else:
        return False
