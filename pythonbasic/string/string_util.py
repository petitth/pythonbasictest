import random


def random_upper(length):
    valid_char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ''.join((random.choice(valid_char) for i in range(length)))


def random_lower(length):
    valid_char = 'abcdefghijklmnopqrstuvwxyz'
    return ''.join((random.choice(valid_char) for i in range(length)))


def random_string(length):
    valid_char = 'abcdefghijklmnopqrstuvwxyz$*#&+=?ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ''.join((random.choice(valid_char) for i in range(length)))


def random_number(length):
    valid_char = '0123456789'
    return ''.join((random.choice(valid_char) for i in range(length)))
