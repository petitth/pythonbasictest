import datetime


def current_date(format=None):
    if format is not None:
        now = datetime.datetime.now()
        return now.strftime(format)
    else:
        return datetime.datetime.now()
