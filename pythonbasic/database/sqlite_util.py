import sqlite3


def get_connection(db_path):
    try:
        return sqlite3.connect(db_path)
    except Exception as ex:
        print("an exception occurred while connecting to sqlite db : {}".format(ex))
        return None


def execute_update(db_path, sql_query):
    try:
        conn = get_connection(db_path)
        c = conn.cursor()
        c.executescript(sql_query)
        conn.commit()
        conn.close()
    except Exception as ex:
        print("an exception occurred while running query : {}".format(ex))


def execute_select(db_path, sql_query):
    try:
        conn = get_connection(db_path)
        conn.row_factory = sqlite3.Row  # get dictionary as result
        c = conn.cursor()
        c.execute(sql_query)
        result = c.fetchall()
        conn.close()
        return result
    except Exception as ex:
        return None
        print("an exception occurred while running query : {}".format(ex))
