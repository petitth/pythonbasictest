def fibonacci(num):
    x = int(num)
    if x == 0 or x == 1:
        return x
    else:
        return fibonacci(x - 1) + fibonacci(x - 2)


def factorial(num):
    x = int(num)
    if x < 1:
        return 1
    else:
        return x * factorial(x - 1)
