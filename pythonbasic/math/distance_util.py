def kilometers_to_miles(input):
    kilometers = float(input)
    return kilometers * 0.6213711922


def miles_to_kilometers(input):
    miles = float(input)
    return miles * 1.609344


def centimeter_to_feet(x):
    centimeter = float(x)
    return centimeter / 30.48


def feet_to_centimeter(x):
    feet = float(x)
    return feet * 30.48


def centimeter_to_inch(x):
    centimeter = float(x)
    return centimeter / 2.54


def inch_to_centimeter(x):
    inch = float(x)
    return inch * 2.54


def yard_to_meter(x):
    yard = float(x)
    return yard * 0.9144


def meter_to_yard(x):
    meter = float(x)
    return meter * 1.0936132983
