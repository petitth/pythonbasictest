import sys


def get_parameter():
    # the user needs to pass one numeric argument to the script
    if len(sys.argv) <= 1:
        print("Please enter a measure to convert !")
        sys.exit(1)
    try:
        return float(sys.argv[1])
    except ValueError:
        print("Input measure is invalid")
        sys.exit(2)


def centimeter_to_feet(x):
    centimeter = float(x)
    return centimeter / 30.48


if __name__ == '__main__':
    try:
        param = get_parameter()
        print("--- start ---")
        result = centimeter_to_feet(param)
        print("{} centimeters = {} feet".format(param, result))
        print("--- done ---")
    except Exception as ex:
        print("an exception occurred : {}".format(ex))