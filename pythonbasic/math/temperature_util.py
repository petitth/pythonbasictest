def celsius_to_fahrenheit(input):
    celsius = float(input)
    return (celsius * 9 / 5) + 32


def fahrenheit_to_celsius(input):
    fahrenheit = float(input)
    return (fahrenheit - 32) * 5 / 9
