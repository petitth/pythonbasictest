import getopt
import sys
import temperature_util


def get_parameters():
    # the user needs to set a parameter with command lines arguments : --input=50
    args = sys.argv[1:]
    options = "hi"
    long_options = ["help", "input="]
    parse_args, parse_values = getopt.getopt(args, options, long_options)

    result = {"input": None}
    # checking each argument
    for arg, val in parse_args:
        if arg in ("-h", "--help"):
            print("This script requires a parameter.")
            sys.exit(1)
        elif arg in ("-i", "--input"):
            result["input"] = val
    return result


if __name__ == '__main__':
    try:
        parameters = get_parameters()
        input = float(parameters["input"])
        print("--- start ---")
        result = temperature_util.celsius_to_fahrenheit(input)
        print("{} celsius = {} fahrenheit".format(input, result))
        print("--- done ---")
    except Exception as ex:
        print("an exception occurred : {}".format(ex))
