import sys


def get_parameter():
    # the user needs to pass one numeric argument to the script
    if len(sys.argv) <= 1:
        print("Please enter a measure to convert !")
        sys.exit(1)
    try:
        return float(sys.argv[1])
    except ValueError:
        print("Input measure is invalid")
        sys.exit(2)


def meter_to_yard(x):
    meter = float(x)
    return meter * 1.0936132983


if __name__ == '__main__':
    try:
        param = get_parameter()
        print("--- start ---")
        result = meter_to_yard(param)
        print("{} meters = {} yards".format(param, result))
        print("--- done ---")
    except Exception as ex:
        print("an exception occurred : {}".format(ex))