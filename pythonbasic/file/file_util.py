import os
import tempfile
import shutil
import zipfile
import json
from urllib.request import urlopen

# use octal prefix to define access rights
access_rights = 0o755


def get_temp_dir():
    return tempfile.gettempdir()


def create_directory(path):
    # will create subfolders if necessary
    if not does_directory_exist(path):
        os.makedirs(path)


def delete_directory(path):
    if os.path.isdir(path):
        shutil.rmtree(path, ignore_errors=True)


def delete_file(path):
    if os.path.isfile(path):
        os.remove(path)


def append_content(path, content):
    # create subfolders if necessary
    dir = os.path.dirname(os.path.realpath(path))
    create_directory(dir)
    # creates file if it does not exist
    file = open(path, "a")
    file.write(content)
    file.write("\n")
    file.close()


def append_lines(path, lines):
    # create subfolders if necessary
    dir = os.path.dirname(os.path.realpath(path))
    create_directory(dir)
    # add lines
    file = open(path, "a")
    for line in lines:
        file.write(line)
        file.write("\n")
    file.close()


def create(path, content):
    # create subfolders if necessary
    dir = os.path.dirname(os.path.realpath(path))
    create_directory(dir)
    # overwrite content if file exists
    file = open(path, "w")
    file.write(content)
    file.write("\n")
    file.close()


def create_from_lines(path, lines):
    # create subfolders if necessary
    dir = os.path.dirname(os.path.realpath(path))
    create_directory(dir)
    # overwrite content if file exists
    file = open(path, "w")
    for line in lines:
        file.write(line)
        file.write("\n")
    file.close()


def get_file_content(path):
    file = open(path, "r")
    content = file.read()
    file.close()
    return content


def get_file_lines(path):
    file = open(path, "r")
    lines = file.readlines()
    file.close()
    return lines


def list_files(dir_path):
    result = list()
    for subdir, dirs, files in os.walk(dir_path):
        for file in files:
            result.append(subdir + os.sep + file)
    return result


def list_content(dir_path):
    result = list()
    for subdir, dirs, files in os.walk(dir_path):
        if not subdir == dir_path:
            result.append(subdir)
        for file in files:
            result.append(subdir + os.sep + file)
    return result


def does_path_exist(path):
    return os.path.exists(path)


def does_file_exist(path):
    return os.path.isfile(path)


def does_directory_exist(path):
    return os.path.isdir(path)


def zip_files(dir_path, output_path):
    files = list_files(dir_path)
    zf = zipfile.ZipFile(output_path, mode='w')
    try:
        for file in files:
            zf.write(file, str(file).replace(dir_path, ""))
    finally:
        zf.close()


def unzip_files(archive_path, output_path):
    try:
        zf = zipfile.ZipFile(archive_path)
        zf.extractall(output_path)
    finally:
        zf.close()


def write_json(dict, output_path):
    try:
        dir = os.path.dirname(os.path.realpath(output_path))
        create_directory(dir)
        with open(output_path, 'w') as result:
            json.dump(dict, result)
    except Exception as ex:
        print("an exception occurred while writing data to json file : {}".format(ex))


def read_json(path):
    try:
        with open(path) as json_data:
            return json.load(json_data)
    except Exception as ex:
        print("an exception occurred while reading json file : {}".format(ex))


def get_file_content_from_url(url):
    try:
        content = urlopen(url).read().decode('UTF-8')
        return content
    except Exception as ex:
        return "cannot read url"
