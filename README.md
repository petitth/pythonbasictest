# Python utilities

This project contains utility methods and tests of basic instructions.

## Python naming convention

```text
module_name, package_name, ClassName, method_name, ExceptionName, function_name, GLOBAL_CONSTANT_NAME,
global_var_name, instance_var_name,function_parameter_name, local_var_name
```

## Install extra packages

To tests instructions like soap calls, charts, you'll have to install extra python packages.  
Install the following packages : `suds`, `plotly` and `kaleido`.

First install missing dependencies :
```bash
apt-get install python3-pip
python3 -m pip config set global.break-system-packages true
# see the new property in ~/.config/pip/pip.conf
pip3 install virtualenv
```

Then install packages in your venv :

Go to `menu | View | Tool Windows | Python Packages`  
then search the package to install and click on `install` button.